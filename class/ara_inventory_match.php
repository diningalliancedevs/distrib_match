<?php  
 require_once "update_database.php" ;

// ara_inventory_match.php
// part of New ARA Inventory Match Tool, 
// ara_inventory_match: matches the sql database tables
// 	generically, so it won't be distrib specific
// sdn Sept-Oct 2017


class ara_inventory_match
{
// this will perform the processing on the inventory data for the SQL database
// will query the various tables to find matching records.
// want to match distributor inventory records to Aramark pricing records

	public $distrib_name = " " ;			
	public $mydate_format = " ";		// tbd, set, not currently used
	public $use_date = " "  ;			// tbd, set, not currently used
	
	public $noisy = false ;
	public $update_db_inst = " "  ;
	public $first_file_date = " "  ;		// tbd, set, not currently used
	public $contract_month_start = " " ;

	public $distrib_inventory = " "  ;
	public $item_pack = "item_pack"  ;
	public $brand_name = "brand"  ;
	public $vendor_name = " "  ;

	public $dbname = "" ;
	public $detail_cnt_size = 0 ;
	
	public $deviation_report_sql ="" ;
	public $deviation_ext_report_sql = "" ;


	/* * * *
	 * init_values
	 * 		initialization, update the table index, prepare for query
	 * 		
	 * return:nothing
	*/
	public function init_values($mydistrib_name)
	{	
		$this->noisy = false ;	
	
		$this->first_file_date  = null ;		// tbd
		$this->contract_month_start = null ;	// tbd
		
		$this->item_pack = "pack";			// normal default
		$this->brand_name = "brand";
		$this->vendor_name = "vendor_name" ;
		
		$this->distrib_name = $mydistrib_name ;		
		$this->distrib_inventory = $this->distrib_name . "_inventory" ;
		$this->dbname = "da_inventories" ;	
	
		if (strncmp($this->distrib_name, "nicholas", 6) == 0)
			$this->item_pack = "item_pack" ;
		if (strncmp($this->distrib_name, "bek", 3) == 0)
			$this->brand_name = "brand_name" ;
		if (strncmp($this->distrib_name, "reinhart", 5) == 0)
			$this->vendor_name = "mfr_name" ;		// missing, so substitute
		$distrib_report = $this->distrib_name . "_deviated_item_report" ; 
		
//--  the sql for the normal report here 
	 $this->deviation_ext_report_sql = <<<EOQ
DROP TABLE IF EXISTS $distrib_report;
CREATE TABLE
$distrib_report
AS
SELECT
ni.din as 'Item',
ni.$this->vendor_name as 'Vendor Name',
ni.$this->item_pack as 'Pack',
ni.uom as 'Size',
ni.$this->brand_name as 'Brand',
ni.item_description as 'Desc',
ni.cleaned_min as 'MFG #',
""  as 'Term Set',
""  as 'ACD',
m.effective_start_date as 'Contract Start',		
m.effective_end_date as 'Contract End',		
m.min as 'Matched MIN',					
m.mfg_name as 'Matched Manufacturer',
m.brand as 'Matched Brand',
m.ara_product_description as 'Matched Product Description',	
m.contracted_price as 'Contracted Price',
m.ara_selling_uom as 'Per',						
m.delivered_fob as 'Price Schedule',		
m.mfg_pack_size as 'Pack Size',
""  as 'Add/Del/Mod',
m.new_contracted_price as 'New Contracted Price',	-- tbd
"" as 'Comments'
FROM
da_inventories.tmp_match_detail as m,
da_inventories.$this->distrib_inventory as ni
WHERE (m.min = ni.cleaned_min) and (m.din = ni.din)
GROUP BY ni.din
ORDER BY item_description;	
COMMIT ;  

EOQ;


// -- must have empty line after EOQ
// -- -- sql for the extended report with houses
	$this->deviation_ext_report_sql = <<<EOQ
DROP TABLE IF EXISTS $distrib_report;
CREATE TABLE
$distrib_report
AS
SELECT
ni.house as 'Division',
ni.din as 'Item',
ni.$this->vendor_name as 'Vendor Name',
ni.$this->item_pack as 'Pack',
ni.uom as 'Size',
ni.$this->brand_name as 'Brand',
ni.item_description as 'Desc',
ni.cleaned_min as 'MFG #',
""  as 'Term Set',
""  as 'ACD',
m.effective_start_date as 'Contract Start',		
m.effective_end_date as 'Contract End',			--
m.min as 'Matched MIN',					-- m.cleaned_min
m.mfg_name as 'Matched Manufacturer',
m.brand as 'Matched Brand',
m.ara_product_description as 'Matched Product Description',		
m.contracted_price as 'Contracted Price',
m.ara_selling_uom as 'Per',		
m.delivered_fob as 'Price Schedule',
m.mfg_pack_size as 'Pack Size',
""  as 'Add/Del/Mod',
m.new_contracted_price as 'New Contracted Price',	-- tbd
"" as 'Comments'
FROM
da_inventories.tmp_match_detail as m,
da_inventories.$this->distrib_inventory as ni
WHERE (m.min = ni.cleaned_min) and (m.din = ni.din)
GROUP BY ni.din
ORDER BY item_description;	
COMMIT ;  

EOQ;

// -- must have empty line after EOQ -- -- end of sql

	}		// end of init_values
		
	
	/* * * *
	 * update_ara_table_index
	 * 		initialization, update the table index, prepare for query
	 * 		
	 * return:nothing
	*/
	public function update_ara_table_index()
	{				
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
		// put index on ara_contracted_pricing_list, *_inventory, master_mfr
		
		$sql = <<<EOQ
-- -- fix up (nicholas) inventory		
 ALTER table da_inventories.$this->distrib_inventory
 	add  cleaned_min varchar(255);
 	add  cleaned_size varchar(255);	
 COMMIT ;	
 UPDATE da_inventories.$this->distrib_inventory ni
 	SET ni.cleaned_min = da_inventories.min_cleanup(min) ;	
 UPDATE da_inventories.$this->distrib_inventory ni
 	SET ni.cleaned_size = da_inventories.min_cleanup($this->item_pack) ;	
 COMMIT ;
 CREATE INDEX ni_cmin ON da_inventories.$this->distrib_inventory (cleaned_min);		
 CREATE INDEX ni_gtin ON da_inventories.$this->distrib_inventory (gtin);
 CREATE INDEX ni_mnm ON da_inventories.$this->distrib_inventory (mfr_name);	
 CREATE INDEX ni_clsz ON da_inventories.$this->distrib_inventory($this->item_pack);
 CREATE INDEX ni_vnm ON da_inventories.$this->distrib_inventory($this->vendor_name); 
 CREATE INDEX ni_csiz ON da_inventories.$this->distrib_inventory (cleaned_size);	
-- fix up ara_contracted_pricing_list 
ALTER table da_inventories.ara_contracted_pricing_list_cp
	add  cleaned_size varchar(255);
	add  cleaned_size_nbr varchar(255);		
COMMIT ;	
UPDATE da_inventories.ara_contracted_pricing_list_cp
	SET cleaned_size = da_inventories.min_cleanup(ara_pack_size) ;	
UPDATE da_inventories.ara_contracted_pricing_list_cp	
	SET cleaned_size_nbr = da_inventories.min_cleanup(ara_pack_size_nbr) ;
COMMIT ; 
 CREATE INDEX cp_br ON da_inventories.ara_contracted_pricing_list_cp(ara_brand_name);
 CREATE INDEX cp_mid ON da_inventories.ara_contracted_pricing_list_cp(ara_manufacturer_id); 
 CREATE INDEX cp_gtin ON da_inventories.ara_contracted_pricing_list_cp(mfg_gtin);
 CREATE INDEX cp_item ON da_inventories.ara_contracted_pricing_list_cp(mfg_item_number); 
 CREATE INDEX cp_nm ON da_inventories.ara_contracted_pricing_list_cp(mfg_name); 
  CREATE INDEX cp_psz ON da_inventories.ara_contracted_pricing_list_cp(mfg_pack_size);  
 CREATE INDEX cp_clcd ON da_inventories.ara_contracted_pricing_list_cp(sector_code);
 CREATE INDEX cp_clnbr ON da_inventories.ara_contracted_pricing_list_cp(cleaned_size_nbr); 
 CREATE INDEX cp_clsz ON da_inventories.ara_contracted_pricing_list_cp(cleaned_size);
 -- set indices on ara_contracted_pricing_list
 CREATE INDEX mm_mid ON da_inventories.master_mfr (ara_manu_id);
 CREATE INDEX mm_vnm ON da_inventories.master_mfr (vendor_name);
 CREATE INDEX mm_vnm ON da_inventories.master_mfr (foodbuy_mfr_name);
COMMIT ;

EOQ;

		if ($this->noisy)							
			print "   update_ara_table_index SQL 1, $sql .\n" ;	
		print "Running update_ara_table_index. This takes couple seconds to run.\n" ;	
		print "		Started at $start_time.\n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
				
		try {
			$dbReply->closeCursor() ;	//
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, update_ara_table_index 86, after update_database, throw exception.\n\n";
			flush() ;
		}				
		print "  -- -- -- -- -- -- -- -- --\n\n" ;	

		flush() ;		
	}		// end of update_ara_table_index


		
	/* * * *
	 * create_copy_of_inv
	 * 	 create a copy of the distributor's inventory
	 * 		will be manipulated during the matching processing
	 *
	 * no parameters
	 * return:nothing
	*/
	public function create_copy_of_inv()
	{	
	
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
		
		// make a local copy of the inventory, 
		// when we match, we'll delete from this list
		// not all have the same name, standardize it here!
		// not all have all the columns, so substitute
		
		$sql = <<<EOQ
SET @ctr := 0;
DROP TABLE IF EXISTS da_inventories.tmp_inventory_items;
CREATE TABLE da_inventories.tmp_inventory_items
AS
SELECT 
@ctr := @ctr + 1 AS unique_id,
c.din, c.$this->brand_name as brand, c.item_description, c.$this->item_pack as item_pack,
c.uom, c.min, c.mfr_name, c.gtin, 
da_inventories.min_cleanup(c.min) as cleaned_min,
da_inventories.min_cleanup(c.$this->item_pack) as cleaned_size,	
-- c.cleaned_min, c.cleaned_size,
c.$this->vendor_name as vendor_name 
FROM
   da_inventories.$this->distrib_inventory as c ;
COMMIT ;
CREATE INDEX ti_id ON da_inventories.tmp_inventory_items(unique_id);
CREATE INDEX ti_br ON da_inventories.tmp_inventory_items(brand);
 CREATE INDEX ti_gtin ON da_inventories.tmp_inventory_items (gtin);
 CREATE INDEX ti_mnm ON da_inventories.tmp_inventory_items (mfr_name);
 CREATE INDEX ti_cmin ON da_inventories.tmp_inventory_items (cleaned_min);
 CREATE INDEX ti_pck ON da_inventories.tmp_inventory_items(item_pack);
 CREATE INDEX ti_vnm ON da_inventories.tmp_inventory_items(vendor_name);
  CREATE INDEX ti_clsz ON da_inventories.tmp_inventory_items(cleaned_size);
 CREATE INDEX ti_min ON da_inventories.tmp_inventory_items(min);
COMMIT ;

EOQ;

		if ($this->noisy)								
			print "   create_copy_of_inv SQL 1, $sql " ;
		print "Running create_copy_of_inv SQL 1, This takes time copy the inventory. About 1 min per 10k.\n" ;	
		print "		Started at $start_time.\n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		try {
			$dbReply->closeCursor() ;
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, create_copy_of_inv 241, after update_database, throw exception.\n\n";
			flush() ;
		}	
		
		//if ($this->noisy)	
		{			
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " create_copy_of_inv: stop_time: $stop_time\n" ;
			print "       		start_time: $start_time\n\n" ;	
		}					
		
		$sql =	"select count(*) as count from da_inventories.tmp_inventory_items; " ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, create_copy_of_inv 273, fetch throw exception.\n";
			flush() ;
		}

		$entries =  $sqlresult["count"] ;
		print "  Number of entries in Copy of Inventory: $entries\n" ;
		if ($this->noisy)
			print "  Returning from create_copy_of_inv function.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n" ;
		if (($this->noisy) && ($entries < 100))
		{	
			print "\nNOT ENOUGH VALUES FOUND, should be at least 100 or more!!\n\n" ;
			flush() ;			
			//die ;
		}
		flush() ;		
	}		// end of create_copy_of_inv
	
		
	/* * * *
	 * setup_ara_price_min_din_inv
	 * 		step 1, create ara_price_min_din_inv from the query
	 *	create the ara_price_min_din_inv by MIN (min, cleaned_min or Gtin) & DIN
	 *
	 * no parameters	 
	 * return:nothing
	*/
	public function setup_ara_price_min_din_inv()
	{	
		
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
		
		if ($this->noisy)								
			print "   setup_ara_price_min_din_inv ck distrib_name, $this->distrib_name\n" ;		
		// look for exact match: Min (min, cleaned_min, gtin), DIN, Manufacturer, size, Vendor
		//
		
		$sql = <<<EOQ
DROP TABLE IF EXISTS da_inventories.ara_price_min_din_inv;
CREATE TABLE da_inventories.ara_price_min_din_inv
AS
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob, ac.ara_selling_uom, --  !!	-- add any additional fields needed for report?	 
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
	 ara_min_din.min_din as md
where  md.distributor_name like "$this->distrib_name%" 						
 	and ac.sector_code like "Restaur%"						
 	and md.ara_master_manufacturer_name = ac.ara_manufacturer_name	
 	and md.ara_master_item_size = ac.ara_item_size_nbr		
 	and md.ara_master_item_pack_size	= ac.ara_pack_size_nbr 	
	and (md.ara_master_item_desc = ac.ara_product_description
		or md.ara_master_item_desc = ac.mfg_item_description)	
	and (md.ara_product_master_id = ac.ara_product_master_id)
	and (ni.min = ac.mfg_item_number					-- min
		 or ni.cleaned_min = ac.mfg_item_number		-- cleaned_min
		 or ni.gtin = ac.mfg_gtin)		               -- gtin
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) ;				-- 19
COMMIT ;
-- after the create!! 
CREATE INDEX cudi_id ON da_inventories.ara_price_min_din_inv(unique_id);
CREATE INDEX cudi_min ON da_inventories.ara_price_min_din_inv(cleaned_min);
CREATE INDEX cudi_mfr ON da_inventories.ara_price_min_din_inv(mfg_name);
CREATE INDEX cudi_gtin ON da_inventories.ara_price_min_din_inv(gtin);
CREATE INDEX cudi_br ON da_inventories.ara_price_min_din_inv(brand);
COMMIT ;				
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
COMMIT ;

EOQ;

		if ($this->noisy)								
			print "   setup_ara_price_min_din_inv SQL 1, $sql" ;
		print "Running setup_ara_price_min_din_inv. This takes a min to run.\n" ;	
		print "		Started at $start_time.\n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		try {
			$dbReply->closeCursor() ;
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, setup_ara_price_min_din_inv 241, after update_database, throw exception.\n\n";
			flush() ;
		}		
		
		//if ($this->noisy)	
		{			
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " setup_ara_price_min_din_inv: stop_time: $stop_time\n" ;
			print "       			start_time: $start_time\n\n" ;	
		}			

		$sql =	"select count(*) as count from da_inventories.ara_price_min_din_inv; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   setup_ara_price_min_din_inv SQL 2, $sql \n" ;								
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, setup_ara_price_min_din_inv 273, fetch throw exception.\n";
			flush() ;
		}

		$entries =  $sqlresult["count"] ;
		print "  Number of entries in ara_price_min_din_inv: $entries\n" ;
		if ($this->noisy)
			print "  Returning from setup_ara_price_min_din_inv function.\n" ;

		if ($entries < 10)
		{	
			print "MIN-DIN selection was not very successful for this distributor.\n" ;
			print "Not many values found, should be 10 or more. WARNING! \n" ;		

		}
		//print " setup_ara_price_min_din_inv 281 returning from setup_ara_price_min_din_inv.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n\n" ;			
		flush() ;		
	}		// end of setup_ara_price_min_din_inv
	
		
	
	/* * * *
	 * update_APMDI_by_min_manu
	 * 
	 *	step 2, update APMDI_by_min_manu for the distributor
	 *	update the ara_price_min_din_inv by MIN (min, cleaned_min or Gtin) & Manufacturer
	 *
	 * no parameters
	 * return:nothing
	*/
	public function update_APMDI_by_min_manu()
	{
		$sql = " " ;
		// look for great match: (min, cleaned_min or Gtin), Manufacturer, size, Vendor
		//		
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
		
		$sql = <<<EOQ
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob,  ac.ara_selling_uom, --  !!	-- add any additional fields needed for report?	 
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.vendor_name = mm.vendor_name 		-- vendor 
		or ni.vendor_name = mm.foodbuy_mfr_name		-- !ADD THIS!*!
		or ni.mfr_name = mm.foodbuy_mfr_name			-- !ADD THIS!*!
			or ni.mfr_name = mm.vendor_name)
	and (ni.min = ac.mfg_item_number								-- match on min
		 or ni.cleaned_min = ac.mfg_item_number				-- match on cleaned_min
		 or ni.gtin = ac.mfg_gtin)		               		-- match on gtin	
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
	 		or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.min, ni.cleaned_size, ni.mfr_name	;		-- 259
-- after the insert, Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
COMMIT ;

EOQ;


		if ($this->noisy)							
			print "   update_APMDI_by_min_manu SQL 1, $sql ";
		print "Running update_APMDI_by_min_manu. This takes 3-10 min to run.\n" ;	
		print "		Started at= $start_time. \n" ;		
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{				
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " update_APMDI_by_min_manu: stop_time: $stop_time\n" ;
			print "      		 	start_time: $start_time\n\n" ;	
		}	

		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();	
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, update_APMDI_by_min_manu 375, throw exception.\n";
			flush() ;
		}

		$sql =	"select count(*) as count from da_inventories.ara_price_min_din_inv; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   update_APMDI_by_min_manu SQL 2, $sql \n" ;						
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, update_APMDI_by_min_manu 393, fetch throw exception.\n";
			flush() ;
		}
		$entries =  $sqlresult["count"] ;
		print "  Number of entries in ara_price_min_din_inv: $entries\n" ;
		if ($this->noisy)
			print "  Returning from update_APMDI_by_min_manu function.\n" ;
		
		if ($entries < 40)
		{	
			print "MIN-Manufacturer selection was not very successful for this distributor.\n" ;
			print "Not many values found, should be 40 or more. WARNING! \n" ;

		}		
		print "  -- -- -- -- -- -- -- -- --\n\n" ;			
		flush() ;		
	}		// end of APMDI_by_min_manu
		

	
	/* * * *
	 * update_APMDI_by_min_brand
	 * 		step 3, update APMDI_by_min_brand for the distributor
	 *	update the ara_price_min_din_inv by MIN (min only) & Brand
	 *
	 * no parameters	 
	 * return:nothing
	*/
	public function update_APMDI_by_min_brand()
	{
		$sql = " " ;
		// look for good match: brand, min, size, manufacturer_id
		//	not vendor.
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
		
		$sql = <<<EOQ
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob,  ac.ara_selling_uom, --  !!	-- add any additional fields needed for report?	 
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.brand = ac.ara_brand_name)				-- brand is less reliable 
	and (ni.min = ac.mfg_item_number)				-- match on min		
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
			or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.cleaned_min, ni.cleaned_size, ni.brand	;		-- 
-- Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
COMMIT ;

EOQ;


		if ($this->noisy)							
			print "   update_APMDI_by_min_brand SQL 1, $sql ";
		print "Running update_APMDI_by_min_brand. This takes 1-2 min to run.\n" ;	
		print "		Started at= $start_time. \n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{		
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " update_APMDI_by_min_brand: stop_time: $stop_time\n" ;
			print "  				 start_time: $start_time\n\n" ;	
		}	

		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, update_APMDI_by_min_brand, close, throw exception.\n";
			flush() ;
		}

		$sql =	"select count(*) as count from da_inventories.ara_price_min_din_inv; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   update_APMDI_by_min_brand SQL 2, $sql \n" ;						
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, update_APMDI_by_min_brand fetch, throw exception.\n";
			flush() ;
		}
		$entries =  $sqlresult["count"] ;
		print "  Number of entries in ara_price_min_din_inv: $entries\n" ;
		if ($this->noisy)
			print "  Returning from update_APMDI_by_min_brand function.\n" ;
		
		if (($this->noisy) && ($entries < 40))
		{	
			print "MIN-BRAND selection was not very successful for this distributor.\n" ;
			print "Not many values found, should be 40 or more. WARNING! \n\n" ;
			flush() ;
			// die ;
		}	
		print "  -- -- -- -- -- -- -- -- --\n\n" ;			
		flush() ;
	}		// end of update_APMDI_by_min_brand
			
		
	/* * * *
	 * update_APMDI_by_clean_min_brand
	 * 		step 4, update_APMDI_by_clean_min_brand, for the distributor
	 *	update the ara_price_min_din_inv by cleaned_min (cleaned_min only) & Brand
	 *
	 * no parameters	 
	 * return:nothing
	*/
	public function update_APMDI_by_clean_min_brand()
	{
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			
		$sql = <<<EOQ
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob, ac.ara_selling_uom, --  !!	-- add any additional fields needed for report?	 
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.brand = ac.ara_brand_name)				-- brand is less reliable 
	and (ni.cleaned_min = ac.mfg_item_number)		-- match on cleaned_min		
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
			or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.cleaned_min, ni.cleaned_size, ni.brand	;		-- 
-- Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
COMMIT ;
	
EOQ;

		if ($this->noisy)							
			print "   update_APMDI_by_clean_min_brand SQL 1, $sql \n" ;
		print "Running update_APMDI_by_clean_min_brand. This takes a min to run.\n" ;	
		print "		Started at $start_time.\n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{		
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " update_APMDI_by_clean_min_brand: stop_time: $stop_time\n" ;
			print "  				   start_time: $start_time\n\n" ;	
		}					
		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();		
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, update_APMDI_by_clean_min_brand 463, throw exception.\n";
			flush() ;
		}

		$sql =	"select count(*) as count from da_inventories.ara_price_min_din_inv; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   update_APMDI_by_clean_min_brand SQL 2, $sql \n" ;								
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, update_APMDI_by_clean_min_brand 478, fetch threw exception.\n";
			flush() ;
		}
		$entries =  $sqlresult["count"] ;
		print "  Number of entries in ara_price_min_din_inv: $entries\n" ;
		if ($this->noisy)
			print "  Returning from update_APMDI_extra function.\n" ;
		
		if (($this->noisy) && ($entries < 40))
		{	
			print "NOT ENOUGH VALUES FOUND, should be 40 or more. WARNING!!\n\n" ;
			//die ;
		}
		print "  -- -- -- -- -- -- -- -- --\n\n" ;	
		flush() ;		
	}		// end of update_APMDI_by_clean_min_brand
	
	
	
	/* * * *
	 * cre_tmp_match_by_perfect
	 * 		step 5, create tmp_match using perfect match, for the distributor
	 *	create the tmp_match by matching the MINs
	 *
	 * no parameters	
	 * return:nothing
	*/
	public function cre_tmp_match_by_perfect()
	{	
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;						

		// create the tmp_match table
		//	match on manufacturer, brand, min
		
		$sql = <<<EOQ
DROP TABLE IF EXISTS da_inventories.tmp_match;
CREATE TABLE da_inventories.tmp_match
AS
SELECT DISTINCT ni.din, pi.unique_id
FROM 	 da_inventories.$this->distrib_inventory as ni,
	da_inventories.ara_price_min_din_inv as pi,	
	da_inventories.master_mfr as mm									
WHERE (ni.cleaned_min = pi.min) 
   and (ni.din = pi.din)				-- ensure same product !
   and (pi.ara_manufacturer_name = mm.ara_manu_id 
	     or pi.mfg_name = mm.ara_manu_id)   
group by ni.cleaned_min ;  
CREATE INDEX merchants_id ON da_inventories.tmp_match(unique_id);
COMMIT ;

EOQ;

		if ($this->noisy)	
			print "    in cre_tmp_match_by_perfect SQL 1, $sql " ;
		print "Running cre_tmp_match_by_perfect. This takes a min to run.\n" ;	
		print "		Started at $start_time.\n" ;
		
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{			
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " cre_tmp_match_by_perfect: stop_time: $stop_time\n" ;
			print "       				start_time: $start_time\n\n" ;	
		}		
		try{
			$dbReply->closeCursor() ;
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {

			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, cre_tmp_match_by_perfect 564, throw exception.\n";
			flush() ;
		}	
		
		$sql =	"select count(*) as count from da_inventories.tmp_match; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)							
			print "    in cre_tmp_match_by_perfect  SQL 2, $sql \n" ;							
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, in cre_tmp_match_by_perfect 580, fetch threw exception.\n";
			flush() ;
		}
		
		$entries = $sqlresult["count"] ;
		print "  Number of rows in tmp_match: $entries\n" ;
		if ($this->noisy) 
			print "  Returning from cre_tmp_match_by_perfect function.\n" ;

		if ($entries < 10)
			print "Not very many values found, should be 10 or more. continuing.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n\n" ;			
		flush() ;
	}		// end of cre_tmp_match_by_perfect
	
	
	/* * * *
	 * upd_tmp_match_by_min_brand
	 * 		step 5A, create the tmp_match, using match on min & brand for the distributor
	 * 		NOT NEEDED AT THE MOMENT
	 * return:nothing
	*/
	public function upd_tmp_match_by_min_brand()
	{	
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;			
		// create the tmp_match table
		// Resummarize tmp_match by dist_item_id to eliminate dups and whittle down to the distributor
		$sql = " " ;
		
		// expect to find about 200
		
		$sql = <<<EOQ
INSERT INTO da_inventories.tmp_match
SELECT
DISTINCT
 ni.din, pi.unique_id
FROM  da_inventories.$this->distrib_inventory as ni,	
	da_inventories.ara_price_min_din_inv as pi,	
	da_inventories.master_mfr as mm								
WHERE						
	(ni.cleaned_min = pi.min) 
	   and (ni.din = pi.din)				-- ensure same product !
group by ni.brand, ni.cleaned_min ; 
COMMIT ;	

EOQ;

		if ($this->noisy)							
			print "   upd_tmp_match_by_min_brand  SQL 1, $sql" ;
		print "Running upd_tmp_match_by_min_brand. This takes a min to run.\n" ;	
		print "		Started at $start_time.\n" ;

		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{			//9-21 @3		
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " upd_tmp_match_by_min_brand: stop_time: $stop_time\n" ;
			print "      		start_time: $start_time\n\n" ;	
		}				
		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, upd_tmp_match_by_min_brand 463, throw exception.\n";
			flush() ;
		}
		
		$sql =	"select count(*) as count from da_inventories.tmp_match; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)							
			print "   upd_tmp_match_by_min_brand  SQL 2, $sql \n" ;								
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, match_by_min_brand 665, fetch threw exception.\n";
			flush() ;
		}
		$entries = $sqlresult["count"] ;
		print "  Number of rows in tmp_match: $entries\n" ;
		if ($this->noisy) 
				print "  Returning from upd_tmp_match_by_min_brand function.\n" ;

		if ($entries < 10)
			print "Not very many values found, should be 10 or more. continuing.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n\n" ;			
		flush() ;

	}		// end of upd_tmp_match_by_min_brand
	

	/* * * *
	 * upd_tmp_match_by_min_manu
	 * 		step 5B, update tmp_match for the distributor
	 * 	NOT NEEDED AT THE MOMENT		
	 * return:nothing
	*/
	public function upd_tmp_match_by_min_manu()
	{
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;			
		// create the tmp_min_tin table
		// Resummarize min_din_summary by dist_item_id to eliminate dups and whittle down to the distributor
		
		$sql = <<<EOQ
INSERT INTO da_inventories.tmp_match
SELECT
DISTINCT
 ni.din, pi.unique_id
FROM da_inventories.$this->distrib_inventory as ni,
	da_inventories.ara_price_min_din_inv as pi,	
	da_inventories.master_mfr as mm							
WHERE			
 	(pi.ara_manufacturer_name = mm.ara_manu_id 
	 	or pi.mfg_name = mm.ara_manu_id)	
   and (ni.din = pi.din)				-- ensure same product !		
  AND (ni.cleaned_min = pi.min) ;
COMMIT ;  

EOQ;

		if ($this->noisy)							
			print "   upd_tmp_match_by_min_manu  SQL 1, $sql " ;
		print "Running upd_tmp_match_by_min_manu. May take a min to run.\n" ;	
		print "		Started at $start_time.\n" ;
		
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{			
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " upd_tmp_match_by_min_brand: stop_time: $stop_time\n" ;
			print "     			  start_time: $start_time\n\n" ;	
		}		
		
		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();	
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, match_by_min_manu 463, throw exception.\n";
			flush() ;
		}
		
		$sql =	"select count(*) as count from da_inventories.tmp_match; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   match_by_min_manu SQL 2, $sql \n" ;							
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, upd_tmp_match_by_min_manu, fetch threw exception.\n";
			flush() ;
		}
		$entries = $sqlresult["count"] ;
		print "  Number of rows in tmp_match: $entries\n" ;
		if ($this->noisy) 
				print "  Returning from upd_tmp_match_by_min_manu function.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n" ;	
		if ($entries < 30)			// the last insert will force minimum count
		{	
			print "NOT ENOUGH VALUES FOUND, need at least 30 or more. WARNING.\n\n" ;
			//die ;
		}		
		flush() ;		
	}		// end of upd_tmp_match_by_min_manu
	

	/* * * *
	 * setup_tmp_match_detail
	 * 		step 6,  setup the tmp_match_detail for the distributor
	 * 		
	 * return:nothing
	*/
	public function setup_tmp_match_detail()
	{
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;			
		// create the tmp_match_detail table
		// Resummarize tmp_match_detail by min
		
		$sql = <<<EOQ
DROP TABLE IF EXISTS da_inventories.tmp_match_detail;
CREATE  TABLE
da_inventories.tmp_match_detail
AS
SELECT	
i.din,			 -- din
i.mfg_name,				-- raw_mfr_name,
i.brand,					-- brand,
i.ara_product_description,		-- product_description,
i.effective_start_date,				-- contract_start,
i.effective_end_date,				-- contract_end,
i.ara_contract_price as contracted_price,
i.ara_contract_price as new_contracted_price,
i.min,		-- cleaned_min,
				-- term_set,
				-- acd,
				-- add_del_mod,
i.delivered_fob, --  'Price Schedule',		--  !!
i.mfg_pack_size, 			-- pack_size,	-- !!
i.ara_selling_uom				-- per
FROM
da_inventories.ara_price_min_din_inv as i,
da_inventories.tmp_match as m
WHERE
m.unique_id = i.unique_id ;	
COMMIT ;
CREATE INDEX nmd_min ON tmp_match_detail(min);
COMMIT ;  

EOQ;

		if ($this->noisy)							
			print "   setup_tmp_match_detail SQL 1: $sql \n" ;	
		print "Running setup_tmp_match_detail. This takes a min to run.\n" ;	
		print "		Started at $start_time.\n" ;

		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		//if ($this->noisy)	
		{	
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print " setup_tmp_match_detail: stop_time: $stop_time\n" ;
			print "      			start_time: $start_time\n\n" ;	
		}		
		
		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);			
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, setup_tmp_match_detail 463, throw exception.\n";
			flush() ;
		}

		$sql =	"select count(*) as count from da_inventories.tmp_match_detail; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   setup_tmp_match_detail  SQL 2, $sql \n" ;								
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, setup_tmp_match_detail 844, fetch throw exception.\n";
			flush() ;
		}
		$entries = $sqlresult["count"] ;
		$this->detail_cnt_size = $entries ;
		print "  Number of rows in tmp_match_detail: $entries\n" ;
		if ($this->noisy) 
			print "  Returning from setup_tmp_match_detail function.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n" ;	
		if ($entries < 40)
		{	
			print "NOT ENOUGH VALUES FOUND, should be 40 or more. WARNING.\n\n" ;
			//die ;
		}
		flush() ;		
	}		// end of setup_tmp_match_detail
		
	
	/* * * *
	 * setup_deviated_item_report
	 * 		step 7, create the tmp_deviated_item_report for the distributor
	 * 		
	 * return:nothing
	*/
	public function setup_deviated_item_report()
	{			
		$new_only = 1 ;
		$distrib_report = $this->distrib_name . "_deviated_item_report" ; 
		
		
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;	
		// create the distrib>_deviated_item_report table
		// Resummarize <distrib>_deviated_item_report by dist_item_id to eliminate dups and whittle down to the distributor
		$sql = $this->deviation_report_sql;
		
		if (strncmp($this->distrib_name, "bek", 3) == 0)	// 10-16
			$sql = $this->deviation_ext_report_sql ;
		if (strncmp($this->distrib_name, "reinhart", 5) == 0)		// 10-16
			$sql = $this->deviation_ext_report_sql ;

		if ($this->noisy)						// wanted brand, too restrictive!
			print "   setup_deviated_item_report SQL 1, $sql " ;
		print "Running setup_deviated_item_report. This takes 3-7 min to run.\n" ;	
		print "		started at $start_time.\n" ;
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		//if ($this->noisy)	
		{
			$stop_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;				
			print "\n setup_deviated_item_report: stop_time: $stop_time\n" ;
			print "    			start_time: $start_time\n\n" ;	
		}		
		
		try{
			// this is needed for multi-line sql
			$dbReply->closeCursor();
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, setup_tmp_deviated_item_report 463, throw exception.\n";
			flush() ;
		}
		
		$sql =	"select count(*) as count from da_inventories.$distrib_report ; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		if ($this->noisy)						
			print "   setup_deviated_item_report SQL 2, $sql \n" ;							
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, setup_deviated_item_report 1021, fetch throw exception.\n";
		}

		$size = $sqlresult["count"] ;
		print "  Number of rows in table $distrib_report: $size\n" ;
		if ($size >= 40)
			print "\n* * * Deviation Report completed successfully * * *\n\n" ;
		else if (($size < 40) && ($size > 0) )
			print "\n* * * Deviation Report completed, but very small * * *\n\n" ;

		if ($this->noisy)	
			print "  Returning from setup_deviated_item_report function.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n" ;	
		if (($this->noisy) && ($sqlresult["count"] < 40))
		{	
			print "NOT ENOUGH VALUES FOUND, should be 40 or more. WARNING.\n\n" ;
			//die ;
		}	
		flush() ;		
	}		// end of setup_deviated_item_report
	

	/* * * *
	 * cleanup_deviated_item_report
	 * 		step 8, cleanup deviated_item_report for the distributor
	 * 		
	 * return:nothing
	*/
	public function cleanup_deviated_item_report()
	{		
		$distrib_report = $this->distrib_name . "_deviated_item_report" ; 
		
		$sql = "DELETE FROM $distrib_report
				WHERE min_cleanup(`Matched Min`) not like CONCAT('%',min_cleanup(`MFG #`),'%')
				AND min_cleanup(`MFG #`) not like CONCAT('%',min_cleanup(`Matched Min`),'%'); " ;
		if ($this->noisy)							
			print "   cleanup_tmp_deviated_item_report SQL 1, $sql \n" ;
		print "Running cleanup_tmp_deviated_item_report. This takes a min to run.\n" ;	

		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		$sql =	"select count(*) as count from da_inventories.$distrib_report; " ;
				
		$dbReply = $this->update_db_inst->run_sql_on_db($sql) ;
		
		if ($this->noisy)						
			print "   cleanup_deviated_item_report SQL 2, $sql \n" ;								
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " ignore, cleanup_deviated_item_report 1081, fetch throw exception.\n";
			flush() ;
		}

		print "\n - - - - Cleaned up deviation report for $this->distrib_name - - - -\n" ;
		print "  Number of rows in $distrib_report: $sqlresult[count].\n" ;
		if ($this->noisy)	
			print "  Returning from cleanup_deviated_item_report function.\n" ;
		print "  -- -- -- -- -- -- -- -- --\n\n" ;	
		flush() ;
	}		// end of cleanup_deviated_item_report

}	// end of generic_inventory_match class
 
