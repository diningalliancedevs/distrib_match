<?php  


// update_database
// part of New ETL Conversion Tool, 
// update_database: updates the sql database tables
// sdn June 2017
// sdn updated Aug 2017, for php7.2
// sdn updated Oct 2017 for matching

class update_database
{
// this will perform the processing on the data for the SQL database
// will query the various tables for existing records, now stored in memory
// will insert records into the databases various tables
// and return pk field as an index from various tables

	public $dbConnection ;
	public $noisy ;

	
	/* * * *
	 * get_dbConnection
	 * 		get & return the dbConnection to the database
	 * 		retrieves the reference, for use by other classes
	 * return: the dbConnection
	*/
	public function &get_dbConnection()
	{			
		//print "   update_database:: get_dbConnection  $this->dbConnection.\n" ;
		return ($this->dbConnection);
	}

	/* * * *
	 * set_dbConnection
	 * 		set the dbConnection to the database, for the class
	 * 		save the reference, for use by other classes
	 * 1. newDbConnection: new dbConnection
	 * return: nothing
	*/
	public function set_dbConnection($newDbConnection)
	{		
		$this->noisy=false ;	
		if ($this->noisy)
		{
			print "  update_database::set_dbConnection!! dump newDBConnection : " ;	
			print_r ($newDbConnection) ;
			print"\n" ;
		}
		$this->dbConnection = $newDbConnection ;		
	}
	
    /* * * *
	* make_db_connection
	* 		makes the dbConnection to the database
	* 1. db_name - name of the db
    * return - the connection
	*  Make the connection that mySql server	
    */
    public function make_db_connection($db_name) 
	{
	
		// want to get the connection to the database, use PDO
		// mysql:host=localhost;port=3306;dbname=sue
		// v01: 'mysql:host=localhost;dbname=edwin;charset=utf8';
		print "  update_database:: make_db_connection with $db_name. * * *\n" ;	
		
		$dsn = 'mysql:host=localhost;dbname=' . $db_name . ';charset=utf8';
		$myConnection = null ;
	// FUTURE: need to run as loader, not sue
		//$username = 'loader';
		//$password = 'loader';
		//$username = 'sue';			// datawarehouse
		//$password = '9dT3PB5K';
		$username = 'snapier';			// jvdw_wh
		$password = 'Jvdw2017!';	

		
		$options = array(
		PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',); 

		try{
			$myConnection = new PDO($dsn, $username, $password, $options);
		}
		catch (PDOException $Exception ) {
			// PHP Fatal Error. Second Argument Has To Be An Integer, But PDOException::getCode Returns A
			// String.
			print $Exception->getMessage();
			print $Exception->getCode();
			echo " may want to stop here, update_database, make_db_connection, throw exception.\n";
			flush() ;
			//  throw new pdoDbException($e);
		}
		// these were highly recommended settings
		// try disabling the emulate.
		//$myConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$myConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
		//  found msg set attrib needed
		$myConnection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);		
		
		
		$this->set_dbConnection($myConnection) ;		
		print "  update_database::make_db_connection, after set myConnection\n" ;	
		//print_r ($myConnection) ;	
		return $myConnection ;
    }			// end of make_connection function
	
		
    /* * * 
	* make_connection
	* 	created this, in case anything calls the orig version
	*	it will call my version with the da_check_test parameter
	*	
    * return - the connection
	*  Make the connection that mySql server	
    */
    public function make_connection() 
	{	
		// want to get the connection to the database, use PDO

		// TODO FUTURE: this does get called, so be prepared!
		//$testdb = 'edwin' ;
		$testdb = 'da_check_test' ;			// 
		
		print "     update_database::make_connection will call  make_connection-- $testdb--------\n";		
		$myConnection = $this->make_connection($testdb) ;
		print_r ($myConnection) ;		
		return $myConnection ;
	}
	

	/*
     * check_insert_result	USED IN ETL_LOAD ONLY
	 *	 	check the result of the insert to the database
	 * 1. dbReply - the result from the sql
	 * 2. dim_syn_key - dim_syn_key
	 * 3. db_name - database to query
	 * 4. table - table to query
	 * return - syn_value - the new number if insert worked
	 */
	function check_insert_result($dbReply, $dim_syn_key, $db_name, $table)
	{
		// after insert, need to see if valid!
		$syn_value = 0 ;
		$count = 0 ;			
		$count = $dbReply->rowCount() ;
		//$this->noisy = true ;	

		if ($count == 1)		// devel dbg 
		{
			//$myConnection = &$this->get_dbConnection() ;
			// this is assuming that 1, means it was success !!
		
			// retrieve the new syn key
			$sql = "select max(" . $dim_syn_key . ") from " . $db_name . "." . $table . "; " ;		// 8/17@6
			
			if ($this->noisy)						
				print "   update_database,check_insert_result SQL: $sql \n" ;	
			$dbReply = $this->run_sql_on_db($sql) ;
					
			//$count = $dbReply->rowCount() ;
			// find dim_syn_key
			try{
				$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
			}
			catch (PDOException $Exception ) {
			// PHP Fatal Error. Second Argument Has To Be An Integer, But PDOException::getCode Returns A
			// String.
				print $Exception->getMessage();
				print $Exception->getCode();
				echo " may want to stop here, update_database::check_insert_result, throw exception.\n";
				flush() ;
				//  throw new pdoDbException($e);
			}
			
			list($colkey, $syn_value) = each($sqlresult);	// find the array		

			if ($syn_value == 0)		// Not found!!
			{
				print "     update_database::check_insert_result, ERROR, dim_syn_key not found ?!? Did insert to sql db fail? \n " ;		
			}
		}
		else
		{
			print "     update_database:: check_insert_result ERROR - INSERT to sql database FAILED! \n" ;
			print "     update_database   reply=$dbReply, dim_sym=$dim_syn_key, name=$db_name, table=$table\n" ;
		}
		return ($syn_value) ;
	}
	

	/* * * *
	 * sql_double_special_char_string		USED IN ETL_LOAD ONLY
	 * 		will look for the special characters and fix them for sql
	 * 		if there are single quotes, will put in 2 single quotes
	 * 1. myStr - the string to fix for sql
	 * return: myStr - string fixed up for sql
	 */
	 
	public function sql_double_special_char_string($myStr)
	{
		// will look for the special characters and fix them for sql
		// if there are single quotes, will put in 2 single quotes
		
		// REPLACE SINGLE QUOTE or Backslash 
		// one site says real_escape ; one site says pdo::quote
		// at bottom this says double them up. This works! 
		$cnt2 = 0 ;
		$cnt3 = 0 ;
		$myStr = str_replace("'", "''", $myStr, $cnt2) ;
		//if ($cnt2 > 0)
		//	print " # # up 175 THE DOUBLE OF QUOTE happened $cnt times!! $myStr\n" ;
		$myStr = str_replace("\\", "\\\\", $myStr, $cnt3) ;
		//if ($cnt3 > 0)
		//	print " # # up 178 THE DOUBLE OF BACKSLASH happened $cnt3 times!! $myStr\n" ;
	
		return $myStr ;
	}
	
	 

	
	/* * * 
	 * run_sql_on_db - 
	 *		does an SQL command to the database, handles exceptions
	 * 1. sql - the string of the command to be run
	 * return: sth - the result
	*/
	public function run_sql_on_db($sql)
	{		
		$sth = null ;			
		
		// this is the original, now call multi with true!		
		$sth = $this->run_multi_sql_on_db($sql, $this->dbConnection, "true") ;
				
		return $sth ;
	}			// end of run_sql_on_db function

	
	
	
	/* * * 
	 * run_multi_sql_on_db - 
	 *		does an SQL command to the database, handles exceptions
	 * 1. sql - the string of the command to be run
	 * 2. myConnection - the DBO connection 
	 * 3. need_buffer - bool, to buffer or not (true for single, false for multiple cmds)
	 * return: sth - the result
	*/
	public function run_multi_sql_on_db($sql, $myConnection, $need_buffer)
	{		
		//$this->noisy = true;
		$sth = null ;				
		// now use the PDO. prepare and exec
		try 
		{
			if ($this->noisy)		//  need to use buffered ?!?
				print "\n     update_database::run_multi_sql_on_db, run the prepare with:  $need_buffer  on \n$sql";		

			// $sth = $myConnection->prepare($sql);		// original way, before 9-21		
			$sth = $myConnection->prepare($sql,  array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => $need_buffer)) ;
			//print "     run_multi_sql_on_db, prepare succeeded!\n";

			if ($this->noisy)			// execute needs an array ! send empty array.	
				print "\n     update_database::run_multi_sql_on_db, after prepare, run the execute!\n";		
			$result = $sth->execute(array()) ;			
		}
		catch (PDOException $Exception ) {
			// PHP Fatal Error.  But PDOException::getCode Returns A
			// String.
			print $Exception->getMessage();
			print $Exception->getCode();
			print "\n    update_database::run_multi_sql_on_db, exec FAILED!\n";
			flush() ;
		}
		if ($this->noisy)		
			print "     update_database, exec done, now leaving run_multi_sql_on_db!\n\n";		
		return $sth ;
	}			// end of run_multi_sql_on_db function



	// DATE_FORMAT for SQL

	/* * * *
	 * format_date1		USED IN ETL_LOAD ONLY
	 *		sql has a specific format, need to put in their format
	 * 1. orig_date - the date in mm/dd/yyyy
	 * return new_format - the date in yyyy-mm-dd
	 */
	public function format_date1 ($orig_date)
	{
		// change the format of the date from mm/dd/yyyy 
		// (or almost anything with slashes)
		// to yyyy-mm-dd
		$myFormatedDate = null ;			
		
		$mydate = date_parse($orig_date);
		//print "   up 480, format_date1, dump mydate: " ;
		//print "  year=$mydate[year], month=$mydate[month], day=$mydate[day]\n" ;
		
		// php new version reuqests quotes 8/17@2
		$myFormatedDate = sprintf ("%04d-%02d-%02d", $mydate['year'], $mydate['month'], $mydate['day']) ;

		return ($myFormatedDate);
	}
	
	
	/* * * * 
	 * format_date2		USED IN ETL_LOAD ONLY
	 *		sql has a specific format, need to put in their format	 
	 * 1. orig_date - the date in mm-dd-yyyy
	 * return new_format - the date in yyyy-mm-dd
	 */
	public function format_date2 ($orig_date)
	{
		// change the format of the date from mm-dd-yyyy 
		// (or almost anything with hyphens)
		// to yyyy-mm-dd
		$myFormatedDate = null ;			
		
		$new_date = str_replace("-", "/", $orig_date) ;
		$mydate = date_parse($new_date);
		
		//print "   up 500, format_date2, dump mydate: " ;		
		//print "  year=$mydate[year], month=$mydate[month], day=$mydate[day]\n" ;
		$myFormatedDate = sprintf ("%04d-%02d-%02d", $mydate['year'], $mydate['month'], $mydate['day']) ;

		return ($myFormatedDate);
	}

	
	/* * * *
	 * commit_to_db
	 * 		perform a commit on the sql server database 
	 * 1. db_name - name of the database
	 * return nothing 
	 */
	 
	public function commit_to_db($db_name)
	{		
		//$myConnection = &$this->get_dbConnection($db_name); // call the &function
		// perform a commit to the sql server for the database 	
		//print "     up 693 # COMMIT. will commit the new values to SQL server.\n" ;

		$sql = "COMMIT ; " ;
		if ($this->noisy)
			print "   - - # up 45 sql:  " . $sql . ".\n" ;		
		
		$dbReply = $this->run_sql_on_db($sql) ;
		//print "    up 698  - - sql returned to commit_to_db.\n " ;	
	}
	 
	 
	/* * * *
	 * get_max_value_from_table
	 * 		return the max id from the specified table 
	 * 1. db_name - name of the database
	 * 2. table_name - name of the table
	 * 3. column - name of the column to get maximum value of
	 * return result - the max value
	 */
	 
	public function get_max_value_from_table($db_name, $table_name, $column)
	{	
		$max_value = null ;				
	
	 	//$this->noisy = true ;		
		//$dbconn2 = &$this->get_dbconnection($db_name) ;		// call &function 7/26@3
		// retrieve the new id from table_name
		$sql = "select max( " . $column . ") from " . $db_name . "." . $table_name . "; " ;
		
		if ($this->noisy)		
			print "   up 734, " . $sql . " \n" ;	
		$dbReply = $this->run_sql_on_db($sql) ;
					
		// find new maximum value for column
		try{
			$sqlresult = $dbReply->fetch(PDO::FETCH_ASSOC);
		}
		catch (PDOException $Exception ) {
		// PHP Fatal Error. Second Argument Has To Be An Integer, But PDOException::getCode Returns A
		// String.
			print $Exception->getMessage();
			print $Exception->getCode();
			echo "may want to stop here, update_database 784, throw exception.\n";
			flush() ;
			//  throw new pdoDbException($e);
		}		
		
		// sqlresult will be an array something like this:
		// (
		//  [max(id)] => 10
		// )
		
		list($colkey, $max_value) = each($sqlresult);	// find the array		
		if ($max_value == 0)		// Not found!!
		{
			print "     up 823, ERROR, $column max not found ?!? Did insert to sql db fail? \n " ;		
		}

		return $max_value ;
	}
	 
	
}		// end of update_database class

?>
	