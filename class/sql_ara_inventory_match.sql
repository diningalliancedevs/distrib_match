-- Match the any inventory Match
-- now using the Aramark database and tables
-- SDN 9/7/17 update to make nicholas more generic

-- init  -- -- --
SET @contract_month_start := '2017-09-01';
SET @first_file_date = '2017-08-06'; -- Include files processed after this date
SET @new_only = 1; -- Set to 1 to include new items, -1 to exclude.  Set to 1 for initial run, -1 afterwards


-- -- fix up (nicholas) inventory
alter table da_inventories.nicholas_inventory
	add  cleaned_min varchar(255);
	add  cleaned_size varchar(255);
commit ;	
UPDATE da_inventories.nicholas_inventory
	SET cleaned_min = da_inventories.min_cleanup(min) ;	
UPDATE da_inventories.nicholas_inventory	
	SET cleaned_size = da_inventories.min_cleanup(item_pack) ;	
commit ;

select count(*) from da_inventories.nicholas_inventory ;			-- 18k	-- might take a min
 CREATE INDEX ni_gtin ON da_inventories.nicholas_inventory (gtin);
 CREATE INDEX ni_mnm ON da_inventories.nicholas_inventory (mfr_name);
 CREATE INDEX ni_cmin ON da_inventories.nicholas_inventory (cleaned_min);
 CREATE INDEX ni_clsz ON da_inventories.nicholas_inventory(item_pack);
 CREATE INDEX ni_vnm ON da_inventories.nicholas_inventory(vendor_name); 

-- fix up ara_contracted_pricing_list
-- make a copy, need to be able to set index's on da_deviations pricing list
select count(*) from da_deviations.ara_contracted_pricing_list ;	-- 194k
alter table da_inventories.ara_contracted_pricing_list_cp
	add  cleaned_size varchar(255);
	add  cleaned_size_nbr varchar(255);	
commit ;	
UPDATE da_inventories.ara_contracted_pricing_list_cp
	SET cleaned_size = da_inventories.min_cleanup(mfg_pack_size) ;	-- TODO add the 2nd one
	SET cleaned_size_nbr = da_inventories.min_cleanup(ara_pack_size_nbr) ;	
commit ; 

 CREATE INDEX cp_br ON da_inventories.ara_contracted_pricing_list_cp(ara_brand_name);
 CREATE INDEX cp_mid ON da_inventories.ara_contracted_pricing_list_cp(ara_manufacturer_id); 
 CREATE INDEX cp_gtin ON da_inventories.ara_contracted_pricing_list_cp(mfg_gtin);
 CREATE INDEX cp_item ON da_inventories.ara_contracted_pricing_list_cp(mfg_item_number); 
 CREATE INDEX cp_nm ON da_inventories.ara_contracted_pricing_list_cp(mfg_name); 
  CREATE INDEX cp_psz ON da_inventories.ara_contracted_pricing_list_cp(mfg_pack_size);  
 CREATE INDEX cp_clcd ON da_inventories.ara_contracted_pricing_list_cp(sector_code);
 CREATE INDEX cp_clsz ON da_inventories.ara_contracted_pricing_list_cp(cleaned_size);


-- set indices on ara_contracted_pricing_list
select count(*) from da_inventories.master_mfr ;
 CREATE INDEX mm_mid ON da_inventories.master_mfr (ara_manu_id);
 CREATE INDEX mm_vnm ON da_inventories.master_mfr (vendor_name);



-- STEP 1 -- -- -- create a copy of the inventory

-- Create a list of all the currently active items 
SET @ctr := 0;
DROP TABLE IF EXISTS da_inventories.tmp_inventory_items;
CREATE TABLE da_inventories.tmp_inventory_items
AS
SELECT 
@ctr := @ctr + 1 AS unique_id,
c.din, c.brand as brand, c.item_description, c.item_pack as item_pack,
c.uom, c.min, c.mfr_name, c.gtin, c.cleaned_min, c.cleaned_size, c.vendor_name
FROM
 da_inventories.reinhart_inventory as c ;
 COMMIT ;
-- set indices on it, to speed up search
CREATE INDEX cudi_id ON da_inventories.tmp_inventory_items(unique_id);
CREATE INDEX cudi_br ON da_inventories.tmp_inventory_items(brand);
 CREATE INDEX ni_gtin ON da_inventories.tmp_inventory_items (gtin);
 CREATE INDEX ni_mnm ON da_inventories.tmp_inventory_items (mfr_name);
 CREATE INDEX ni_cmin ON da_inventories.tmp_inventory_items (cleaned_min);
 CREATE INDEX ni_clsz ON da_inventories.tmp_inventory_items(item_pack);
 CREATE INDEX ni_vnm ON da_inventories.tmp_inventory_items(vendor_name); 
COMMIT ;
select count(*) from tmp_inventory_items ;		-- 18117

-- STEP 1 -- -- --
-- where did the min-din go ??

--  search on MIN (min, cleaned min) , DIN, size and sector

--  search on MIN (min, cleaned min, gtin) , DIN, manufacturer, size and sector
DROP TABLE IF EXISTS da_inventories.ara_price_min_din_inv;
CREATE TABLE da_inventories.ara_price_min_din_inv
AS
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,				
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
	 ara_min_din.min_din as md
where  md.distributor_name like "NICH%" 						
 	and ac.sector_code like "Restaur%"						
 	and md.ara_master_manufacturer_name = ac.ara_manufacturer_name	
 	and md.ara_master_item_size = ac.ara_item_size_nbr		
 	and md.ara_master_item_pack_size	= ac.ara_pack_size_nbr 	
	and (md.ara_master_item_desc = ac.ara_product_description
		or md.ara_master_item_desc = ac.mfg_item_description)	
	and (md.ara_product_master_id = ac.ara_product_master_id)
	and (ni.min = ac.mfg_item_number						-- min
		 or ni.cleaned_min = ac.mfg_item_number		-- cleaned_min
		 or ni.gtin = ac.mfg_gtin)		               -- gtin
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) ;				-- 19
				-- 
-- after the create!! 
CREATE INDEX cudi_id ON da_inventories.ara_price_min_din_inv(unique_id);
CREATE INDEX cudi_min ON da_inventories.ara_price_min_din_inv(cleaned_min);
CREATE INDEX cudi_mfr ON da_inventories.ara_price_min_din_inv(mfg_name);
CREATE INDEX cudi_gtin ON da_inventories.ara_price_min_din_inv(gtin);
CREATE INDEX cudi_br ON da_inventories.ara_price_min_din_inv(brand);
COMMIT ;				
				
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
select count(*) from tmp_inventory_items ;		-- 18100
				

-- STEP 3 -- -- --
-- Create a list of all the currently active items for all of dining alliance
-- so this is the big match between inventory and ara_pricing
-- - - -- match on most (min, cleaned min, gtin) , manufacturer, vendor, size
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob, ac.mfg_pack_size, --  !!	-- add any additional fields needed for report?
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.vendor_name = mm.vendor_name 		-- vendor 
		or ni.vendor_name = mm.foodbuy_mfr_name		-- !ADD THIS!*!
		or ni.mfr_name = mm.foodbuy_mfr_name			-- !ADD THIS!*!
			or ni.mfr_name = mm.vendor_name)
	and (ni.min = ac.mfg_item_number								-- match on min
		 or ni.cleaned_min = ac.mfg_item_number				-- match on cleaned_min
		 or ni.gtin = ac.mfg_gtin)		               		-- match on gtin	
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
	 		or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.min, ni.cleaned_size, ni.mfr_name	;		-- 259

-- after the insert, Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
select count(*) from tmp_inventory_items ;		-- 17841

-- add a sweep insert of gtin, manufacturer, vendor, size
-- INSERT INTO da_inventories.ara_price_min_din_inv
-- select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description,
-- ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
-- ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price,
-- ac.ara_item_unit_of_measure_cd,
--	 ac.delivered_fob, ac.mfg_pack_size, --  !!	-- add any additional fields needed for report?
-- ni.din, ni.brand, ni.item_description,
-- ni.item_pack, ni.uom,
-- ni.cleaned_min, ni.gtin, ni.unique_id
-- from  da_inventories.ara_contracted_pricing_list_cp as ac,
-- da_inventories.tmp_inventory_items as ni,
-- da_inventories.master_mfr as mm		--
-- where
-- (ac.mfg_gtin = ni.gtin)					-- gtin may sometimes match
-- and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
-- or ac.mfg_name = mm.ara_manu_id)
-- and (ni.vendor_name = mm.vendor_name 			-- vendor
--		or ni.vendor_name = mm.foodbuy_mfr_name		-- !ADD THIS!*!
--		or ni.mfr_name = mm.foodbuy_mfr_name			-- !ADD THIS!*!
-- or ni.mfr_name = mm.vendor_name)
-- and (ni.cleaned_size =  ac.cleaned_size_nbr
-- or ni.cleaned_size = ac.ara_pack_size_nbr)			-- size
-- and (ac.sector_code like "Restaur%") 			-- 65, 562  found, added 281
-- group by ni.gtin, ni.cleaned_size, ni.mfr_name;			-- 0, 19, 39, 64
-- 
-- 
-- -- Drop the matches from deviated items
-- DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
-- select count(*) from tmp_inventory_items ;		-- 17888
-- 

-- STEP 4 -- -- --
--  search on BRAND, Min (cleaned min), manufacturer and size
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,	
	 ac.delivered_fob, ac.mfg_pack_size, --  !!	-- add any additional fields needed for report?
 	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, 
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.brand = ac.ara_brand_name)				-- brand is less reliable 
	and (ni.cleaned_min = ac.mfg_item_number)		-- match on cleaned_min	
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
			or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.cleaned_min, ni.cleaned_size, ni.brand	;		-- 
				-- found an additional 122 !!

-- Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
select count(*) from tmp_inventory_items ;		-- 17719

-- --- -- -- --- -- -- --- try the other patterns -- -- -- --

-- do the search my min (not cleaned min), manufacturer, vendor, size 
-- INSERT INTO da_inventories.ara_price_min_din_inv
-- select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description,
-- ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
-- ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price,
-- ac.ara_item_unit_of_measure_cd,	
--	 ac.delivered_fob, ac.mfg_pack_size, --  !!	-- add any additional fields needed for report?
-- ni.din, ac.ara_brand_name  as brand, ni.item_description,
-- ni.item_pack, ni.uom,
-- ac.mfg_item_number as min, ni.gtin, ni.unique_id
-- from  da_inventories.ara_contracted_pricing_list_cp as ac,
-- da_inventories.tmp_inventory_items as ni,
-- da_inventories.master_mfr as mm
-- where  (ac.sector_code like "Restaur%")
-- and (ni.vendor_name = mm.vendor_name 				-- vendor
--		or ni.vendor_name = mm.foodbuy_mfr_name		-- !ADD THIS!*!
--		or ni.mfr_name = mm.foodbuy_mfr_name			-- !ADD THIS!*!
-- or ni.mfr_name = mm.vendor_name)
-- and (ni.min = ac.mfg_item_number)					-- min=210
-- and (ac.ara_manufacturer_id = mm.ara_manu_id 	-- manufacturer
-- or ac.mfg_name = mm.ara_manu_id)
-- and (ni.cleaned_size = ac.cleaned_size_nbr
-- or ni.cleaned_size = ac.cleaned_size) 				-- size
-- group by ni.cleaned_min, ni.cleaned_size, ni.mfr_name	;		-- 0
-- 
-- Drop the matches from deviated items
-- DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
-- select count(*) from tmp_inventory_items ;		-- 17749


-- STEP 5 -- -- --

--  search on BRAND, min (not cleaned min) , manufacturer and size
INSERT INTO da_inventories.ara_price_min_din_inv
 select distinct ac.mfg_name, ac.ara_manufacturer_name, ac.mfg_item_description, 
	 ac.ara_product_description, ac.effective_start_date, ac.effective_end_date,
	 ac.mfg_pack_size, ac.ara_pack_size_nbr, ac.ara_contract_price, 
	 ac.ara_item_unit_of_measure_cd,
	 ac.delivered_fob, ac.mfg_pack_size, --  !!	-- add any additional fields needed for report? 
	 ni.din, ac.ara_brand_name as brand, ni.item_description,
	 ni.item_pack, ni.uom, ac.
	 ac.mfg_item_number as min, ni.gtin, ni.unique_id
 from  da_inventories.ara_contracted_pricing_list_cp as ac,
 	 da_inventories.tmp_inventory_items as ni,
 	 da_inventories.master_mfr as mm		  
  where  (ac.sector_code like "Restaur%")
	and (ni.brand = ac.ara_brand_name)				-- brand is less reliable 
	and (ni.min = ac.mfg_item_number)				-- min=
	and (ac.ara_manufacturer_id = mm.ara_manu_id 		-- manufacturer
			or ac.mfg_name = mm.ara_manu_id)	
 	and (ni.cleaned_size = ac.cleaned_size_nbr
	 		or ni.cleaned_size = ac.cleaned_size) 				-- size	 
group by ni.cleaned_min, ni.cleaned_size, ni.brand	;		-- 23
				-- 

-- Drop the matches from deviated items
DELETE FROM tmp_inventory_items WHERE unique_id IN (select unique_id FROM ara_price_min_din_inv);
select count(*) from tmp_inventory_items ;		-- 17696


-- STEP 5 -- -- --

-- now check for / delete duplicates.			-- TODO MORE
SELECT gtin, count(*)
FROM da_inventories.ara_price_min_din_inv
GROUP BY gtin
HAVING COUNT(*) > 1;				-- down to 4 duplicates
	
-- 00049800139183, 00049800164406, 00049800843363, 10072586600001


-- STEP 6 -- -- -- create tmp_match 

-- Now the easy match -  exact - din, min, manu, uom
DROP TABLE IF EXISTS da_inventories.tmp_match;
CREATE TABLE da_inventories.tmp_match
AS
SELECT
 ni.din, pi.unique_id				-- won't this pull all? why do any others?
FROM  da_inventories.nicholas_inventory as ni,
	da_inventories.ara_price_min_din_inv as pi,	
	da_inventories.master_mfr as mm									
WHERE		
   (ni.cleaned_min = pi.min) 		-- 189 
-- (pi.ara_manufacturer_name = mm.ara_manu_id 		-- TODO more!
-- or pi.mfg_name = mm.ara_manu_id)
-- AND (ni.cleaned_min = pi.cleaned_min) 		-- 7 vs 107, 42, 410, 373, prev 60
group by mm.ara_manu_id, ni.brand, ni.cleaned_min ; 	


CREATE INDEX merchants_id ON da_inventories.tmp_match(unique_id);
commit ;

-- INSERT INTO da_inventories.tmp_match		
-- SELECT ni.din, pi.unique_id
-- FROM 	 da_inventories.nicholas_inventory as ni,
-- da_inventories.ara_price_min_din_inv as pi,
-- da_inventories.master_mfr as mm
-- WHERE
-- (pi.ara_manufacturer_name = mm.ara_manu_id 		-- TODO more!
-- or pi.mfg_name = mm.ara_manu_id)
-- AND (ni.cleaned_min = pi.cleaned_min) 		-- 7 vs 107, 42, 410, 373, prev 60
-- group by mm.ara_manu_id, ni.brand, ni.cleaned_min ; 		-- 68, 58
--  


-- part of step on selection
-- Simple match on min
		-- 479, 		
			
-- part of step on selection
-- Simple match on manufacturer + min
-- INSERT INTO da_inventories.tmp_match
-- SELECT
-- ni.din, pi.unique_id
-- FROM da_inventories.nicholas_inventory as ni,
-- da_inventories.ara_price_min_din_inv as pi,
-- da_inventories.master_mfr as mm
-- WHERE
-- (pi.ara_manufacturer_name = mm.ara_manu_id
-- or pi.mfg_name = mm.ara_manu_id)
-- AND (ni.cleaned_min = pi.min) 		-- 7 vs 107, 46, 586, 545, prev33
-- group by ni.brand, ni.cleaned_min ; 		-- 68, 58, 6

select count(*) from tmp_match ;			-- 479
-- now check for / delete duplicates.			---
SELECT unique_id, count(*)
FROM da_inventories.tmp_match
GROUP BY din
HAVING COUNT(*) > 1;					-- 0 dups.		
			
-- NEW STEP 
-- trying to determine if this is helpful					  	
	-- this gives an overlap between the 3 tables. Shows only whatever is in common between all 3.
-- DROP TABLE IF EXISTS da_inventories.tmp_exact_match_from_3;
-- CREATE TABLE da_inventories.tmp_exact_match_from_3
-- AS
-- SELECT md.distributor_item_nbr, md.reported_manufacturer,
-- md.reported_brand, md.reported_item_description,
-- md.reported_mfg_item_nbr, md.reported_upc,
-- md.ara_product_master_id, md.ara_master_manufacturer_name,
-- md.ara_master_brand_name, md.ara_master_item_desc,
-- md.ara_master_mfg_item_nbr, md.ara_master_upc,
-- md.gtin, md.din, md.brand, md.item_description,
-- md.uom, md.mfr_name,
-- ap.mfg_name, ap.ara_manufacturer_name,
-- ap.mfg_item_description, ap.ara_product_description,
-- ap.item_description as ap_item_desc, ap.item_pack,
-- ap.mfg_pack_size, ap.ara_pack_size_nbr,
-- ap.ara_contract_price, ap.uom as ap_uom,
-- ap.cleaned_min
-- FROM da_inventories.ara_min_din_inv as md,
-- da_inventories.ara_price_curr_inv as ap
-- where md.distributor_item_nbr = ap.din
-- and md.inv_gtin = ap.gtin ;
-- 	


-- STEP 6 -- -- -- create tmp_match_detail
DROP TABLE IF EXISTS da_inventories.tmp_match_detail;
CREATE  TABLE
da_inventories.tmp_match_detail
AS
SELECT	-- din -- dist_item_id
i.mfg_name,				-- raw_mfr_name,
i.brand,					-- brand,
i.ara_product_description,		-- product_description,
i.effective_start_date,				-- contract_start,
i.effective_end_date,				-- contract_end,
i.ara_contract_price as contracted_price,
i.ara_contract_price as new_contracted_price,
i.min,		-- cleaned_min,
				-- term_set,
				-- acd,
				-- add_del_mod,
i.delivered_fob, --  'Price Schedule',		--  !!
i.mfg_pack_size, 			-- pack_size,	-- !!
				-- per
FROM
da_inventories.ara_price_min_din_inv as i,
da_inventories.tmp_match as m
WHERE
m.unique_id = i.unique_id 			-- 
GROUP BY m.unique_id ;				-- 419


CREATE INDEX nmd_min ON tmp_match_detail(cleaned_min);		-- din or dist_item_id?


-- NEW STEP 15 -- -- --

-- Check for dup item #s
/*
SELECT 
dist_item_id, count(*)
FROM
tmp_match_detail
GROUP BY
dist_item_id
HAVING COUNT(*) > 1;				-- 0
*/
-- SET @contract_month_start := '2017-09-01';
-- SET @first_file_date = '2017-08-06'; -- Include files processed after this date
-- SET @new_only = 1; -- Set to 1 to include new items, -1 to exclude.  Set to 1 for initial run, -1 afterwards
-- 


-- STEP 7 -- -- -- create the report

-- Now we create the deviated item table for the report to send to Pfg_S
DROP TABLE IF EXISTS tmp_deviated_item_report;
CREATE TABLE
tmp_deviated_item_report
AS
SELECT
ni.din as 'Item',
ni.vendor_name as 'Vendor Name',
ni.item_pack as 'Pack',
ni.uom as 'Size',
ni.brand as 'Brand',
ni.item_description as 'Desc',
ni.cleaned_min as 'MFG #',
""  as 'Term Set',
""  as 'ACD',
m.effective_start_date as 'Contract Start',
m.effective_end_date as 'Contract End',			
m.min as 'Matched MIN',					-- m.cleaned_min
m.brand as 'Matched Brand',
m.ara_product_description as 'Matched Product Description',	
m.mfg_name as 'Matched Manufacturer',
m.contracted_price as 'Contracted Price',
ni.uom  as 'Per',									--  !!
m.delivered_fob as 'Price Schedule',		--  !!
m.mfg_pack_size as 'Pack Size',
"" as 'Add/Del/Mod',
m.new_contracted_price as 'New Contracted Price',	-- TBD !!
'' as 'Comments'
FROM
da_inventories.tmp_match_detail as m,
da_inventories.nicholas_inventory as ni
WHERE m.cleaned_min = ni.cleaned_min
ORDER BY item_description;					-- 486
			

-- STEP 7 -- -- --
-- Clean up bad min/din matches
DELETE *
FROM tmp_deviated_item_report
WHERE
min_cleanup(`Matched Min`) not like CONCAT('%',min_cleanup(`MFG #`),'%')
AND
min_cleanup(`MFG #`) not like CONCAT('%',min_cleanup(`Matched Min`),'%');			-- orig del 86	-- new del 0
			-- 0