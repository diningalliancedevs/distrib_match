<?php  
 require "update_database.php" ;
  require "ara_inventory_match.php" ;

// distrib_match_tool.php
// upper level of the New ARA Inventory Match Tool,
// will be told what files need to get loaded. create cmds
// generically, and substitute in the distrib name.
// sdn Sept-Oct 2017



class distrib_match_tool
{						// distrib_match_tool

	/* * * *
	 * start
	 * the main kickoff for the loading of files
	 * 1. distrib_name - capture, check, etc
	 * 2. month - ## - 1-12, num of month
	 * 3. year - #### - 1990-20xx, 4 digit year
	 * 4. test - # - 1 means noisy=true
	 */ 
	public function start($distrib_name, $month, $year, $test)
	{
		
		$gen_inv_match = new ara_inventory_match() ;
		$gen_inv_match->init_values($distrib_name) ;
		
		if ($test == 1)
			$gen_inv_match->noisy = true ;
		
		// build up the mydate_format and the use_date array
		$this->set_mydate($gen_inv_match, $month, $year) ;	
	
		if ($gen_inv_match->noisy)		
		{		
			print " distrib_match_tool dbname=$gen_inv_match->dbname, distrib=$gen_inv_match->distrib_name!\n" ;
			print " distrib_match_tool mydate_format=$gen_inv_match->mydate_format.\n" ;
			print " - - - - - - - - - - - - - - - - - - - - \n\n" ;
		}
		
		// now call the functions and classes to do the loads!
				
		$update_db = new update_database() ;		// instance
		$start_time = date('y-m-d h:i:s'). substr((string)microtime(), 1, 6) ;	
		$gen_inv_match->update_db_inst = $update_db ;
	
		if ($gen_inv_match->noisy)		
		{
			$num = memory_get_usage() ;		
			print " distrib_match_tool Start: Amount of mem used $num * * * * *\n" ;	// 8/11
			print " distrib_match_tool call create_load_master, with dbname:$gen_inv_match->dbname, $start_time!\n" ;
		}
		$update_db->make_db_connection($gen_inv_match->dbname) ;
		
		if ($gen_inv_match->noisy)		
			print " distrib_match_tool ---------------------start of match processing for $distrib_name ----------------\n" ;
		$StartMatchTime = date('h:i:s'). substr((string)microtime(), 1, 6) ;		
		
		// now start doing all the steps
		// instance

		$gen_inv_match->update_ara_table_index() ;			// initialize
	
		$gen_inv_match->create_copy_of_inv() ;			// copy inventory
		$gen_inv_match->setup_ara_price_min_din_inv() ;		// step 1
	
		$gen_inv_match->update_APMDI_by_min_manu() ;		// step 2
		$gen_inv_match->update_APMDI_by_min_brand() ;		// step 3		
		$gen_inv_match->update_APMDI_by_clean_min_brand() ;	// step 4
	
		$gen_inv_match->cre_tmp_match_by_perfect() ;		// step 5	
			//$gen_inv_match->upd_tmp_match_by_min_brand() ;			// step 5A
			//$gen_inv_match->upd_tmp_match_by_min_manu() ;				// step 5B
		
		$gen_inv_match->setup_tmp_match_detail() ;		// step 6		
	
		$gen_inv_match->setup_deviated_item_report() ;	// step 7

		$gen_inv_match->cleanup_deviated_item_report() ;	// step 8
		$update_db->commit_to_db($gen_inv_match->dbname) ;	
		
		if ($gen_inv_match->noisy)		
			print " distrib_match_tool  ---------------------end of match processing for $distrib_name----------------\n" ;
		
		$EndMatchTime = date('h:i:s'). substr((string)microtime(), 1, 6) ;		
		print "EndMatchTime  = $EndMatchTime\n" ;
		print "StartMatchTime= $StartMatchTime.\n\n" ;			

		print "# # # # # # $distrib_name Match Process Complete  # # # # # \n\n" ;
		flush() ;
	}	// end of start function


	/* * * *
	 * get_mydate
	 * 	  will determine the mydate_format and use_date array 
	 * 1. gen_inv_match - ptr to match class instance
	 * 2. month
	 * 3. year
	 * return nothing
	 */		
	public function set_mydate($gen_inv_match, $month, $year) 		
	{			
		$month2 = sprintf("%02d", $month);		// need 2 digits			
		$year4 = sprintf("%04d", $year);		// need 4 digits
		$year2 = substr($year4, -2 );		// need 2 digits	

		// for inventory, format: 2013-08-31
		// TODO figure out what the correct day is, for end of the month
		$day = "28" ;				// hardcode to something valid for now
			
			
		$new_date_format = $year4 . "-" . $month2 . "-" . $day ;
		$gen_inv_match->mydate_format = $new_date_format ;
				
		$new_use_date = Array ($month2, $year4) ;
		$gen_inv_match->use_date = $new_use_date ;
		
			// find the date: year, month, day	ie: "2013-08-31"
		if ($gen_inv_match->noisy){
			$test =  $gen_inv_match->mydate_format ;
			print "   set_mydate	set mydate_format $test.\n" ;						
			$test =  $gen_inv_match->use_date ;
			print "   set_mydate	set use_Date $test[0].\n" ;						
		}
		
		// contract_start format: 2017-09-01
		$gen_inv_match->contract_month_start = $new_date_format ;	// tbd
		// file_mtime format: 2013-08-31		
		$gen_inv_match->first_file_date  = $new_date_format ;		// tbd			
			
	}	

	
}	// end of distrib_match_tool class
 
?>