-- verify the info in master_mfr
-- these are all valid the first time created in Sept 2017

select count(*) from master_mfr ;			-- 1834
-- 208 + 594 + 42 + 102 + 74 + 38 + 425 + 352 = 1835


select count(*) from master_mfr where foodbuy_mfr_id = '' ;			-- 256

select count(*) from master_mfr where foodbuy_mfr_name = '' ;			-- 256
	
		
select count(*) from master_mfr where vendor_name is null ;			-- 595
	
select count(*) from master_mfr where vendor_num is null ;			-- 1300


-- saladinos has entries with no mfr_id's
	SELECT DISTINCT fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name
	from saladinos_mfr_match
	where fb_mfr_id not in							-- orig found 352		-- note many
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr) ;
		
	SELECT DISTINCT fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name
	from saladinos_mfr_match
	where fb_mfr_id = '';						-- 255

	SELECT DISTINCT fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name
	from saladinos_mfr_match
	where fb_mfr_name = '';						-- 255