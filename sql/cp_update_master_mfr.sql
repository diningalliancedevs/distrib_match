CREATE TABLE master_mfr_cp2 LIKE master_mfr_cp;
INSERT INTO
master_mfr_cp2
SELECT * FROM
master_mfr_cp;

ALTER TABLE da_inventories.master_mfr_cp
   ADD ara_manu_id VARCHAR(255);
   

UPDATE master_mfr_cp2 m INNER JOIN da_salesforce.salesforce_mfr s
SET m.ara_manu_id = s.ara_manufacturer_id
WHERE
m.foodbuy_mfr_id = s.foodbuy_mfr_id ;


select count(*) from da_inventories.master_mfr ;
 CREATE INDEX mm_mid ON da_inventories.master_mfr_cp2 (ara_manu_id);
 CREATE INDEX mm_vnm ON da_inventories.master_mfr_cp2 (vendor_name);
