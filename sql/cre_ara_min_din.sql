-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP TABLE IF EXISTS ara_min_din.ara_min_din

-- Dumping structure for table da_usage_test.ara_min_din_test
CREATE TABLE IF NOT EXISTS ara_min_din.ara_min_din(
  `distributor_id` varchar(3) DEFAULT NULL,
  `distributor_name` varchar(26) DEFAULT NULL,
  `distribution_center_id` varchar(3) DEFAULT NULL,
  `distribution_center_name` varchar(26) DEFAULT NULL,
  `distributor_item_nbr` bigint(20) DEFAULT NULL,
  `reported_manufacturer` varchar(21) DEFAULT NULL,
  `reported_brand` varchar(13) DEFAULT NULL,
  `reported_item_description` varchar(16) DEFAULT NULL,
  `reported_mfg_item_nbr` bigint(20) DEFAULT NULL,
  `reported_upc` bigint(20) DEFAULT NULL,
  `reported_item_selling_uom` varchar(4) DEFAULT NULL,
  `reported_pack_size` bigint(20) DEFAULT NULL,
  `reported_item_uom` varchar(3) DEFAULT NULL,
  `reported_item_size` bigint(20) DEFAULT NULL,
  `ara_product_master_id` bigint(20) DEFAULT NULL,
  `ara_master_manufacturer_id` varchar(8) DEFAULT NULL,
  `ara_master_manufacturer_name` varchar(22) DEFAULT NULL,
  `ara_master_brand_id` bigint(20) DEFAULT NULL,
  `ara_master_brand_name` varchar(9) DEFAULT NULL,
  `ara_master_item_desc` varchar(20) DEFAULT NULL,
  `ara_master_mfg_item_nbr` bigint(20) DEFAULT NULL,
  `ara_master_upc` bigint(20) DEFAULT NULL,
  `ara_major_category_code` bigint(20) DEFAULT NULL,
  `ara_major_category_desc` varchar(10) DEFAULT NULL,
  `ara_minor_category_code` bigint(20) DEFAULT NULL,
  `ara_minor_category_desc` varchar(15) DEFAULT NULL,
  `ara_master_item_selling_uom` varchar(4) DEFAULT NULL,
  `ara_master_item_pack_size` bigint(20) DEFAULT NULL,
  `ara_master_item_uom` varchar(2) DEFAULT NULL,
  `ara_master_item_size` float DEFAULT NULL,
  `gtin` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table da_usage_test.ara_min_din_test: ~0 rows (approximately)
/*!40000 ALTER TABLE `ara_min_din_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `ara_min_din_test` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
