CREATE TABLE da_inventories.ara_contracted_pricing_list_cp LIKE da_deviations.ara_contracted_pricing_list;
INSERT INTO
da_inventories.ara_contracted_pricing_list_cp
SELECT * FROM
da_deviations.ara_contracted_pricing_list;


select count(*) from da_inventories.ara_contracted_pricing_list_cp ;			-- 194k

 CREATE INDEX cp_gtin ON da_inventories.ara_contracted_pricing_list_cp(mfg_gtin);
 CREATE INDEX cp_id ON da_inventories.ara_contracted_pricing_list_cp(mfg_item_number);
 CREATE INDEX cp_br ON da_inventories.ara_contracted_pricing_list_cp(ara_brand_name);
 CREATE INDEX cp_mid ON da_inventories.ara_contracted_pricing_list_cp(ara_manufacturer_id); 
