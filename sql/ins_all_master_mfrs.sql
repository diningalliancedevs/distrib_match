-- find all the ones to add to a master

-- bek_curr_mfr has 2585, so lets start with that for master
-- and add to it

DROP TABLE IF EXISTS master_mfr ;

create table  IF NOT EXISTS master_mfr(
  `foodbuy_mfr_id` varchar(15) DEFAULT NULL,
  `foodbuy_mfr_name` varchar(30) DEFAULT NULL,
  `vendor_num` varchar(20) DEFAULT NULL,
  `vendor_name` varchar(40) DEFAULT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;					-- complete
  

INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)
		SELECT foodbuy_mfr_id, foodbuy_mfr_name, bek_mfr_name
		FROM bek_mfr
		GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, bek_mfr_name ;			-- 208
		
commit ;
select count(*) from master_mfr ;
-- now get all the data from the rest

-- find all the ones to add to master from fsa_mfr_match
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name)
	SELECT   mfr_id, Manufacturer
	FROM fsa_mfr_match
	GROUP BY mfr_id, Manufacturer;								-- 716 added


	-- find all the ones to add to master from maines
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)	
	SELECT foodbuy_mfr_id, foodbuy_mfr_name, maines_vendor_num, maines_vendor_name
	from maines_mfr
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, maines_vendor_num, maines_vendor_name;			-- 100 added
	
commit ;
select count(*) from master_mfr ;

	
	-- find all the ones to add to master from merchants
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT  foodbuy_mfr_id, foodbuy_mfr_name, merchants_mfr_num, merchants_mfr_name
	from merchants_mfr
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, merchants_mfr_num, merchants_mfr_name ;			-- 315 added
		
	-- find all the ones to add to master from nicholas
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)		
	SELECT  foodbuy_mfr_id, foodbuy_mfr_name, nicholas_mfr_name
	from nicholas_mfr
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, nicholas_mfr_name ;			-- 285 added

commit ;
select count(*) from master_mfr ;

	
	-- find all the ones to add to master from pfg_az
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT  foodbuy_mfr_id, foodbuy_mfr_name, pfg_az_mfr_num, pfg_az_mfr_name
	from pfg_az_mfr
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, pfg_az_mfr_num, pfg_az_mfr_name ;			-- 89 added
 
	-- find all the ones to add to master from reinhart
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)		
	SELECT  foodbuy_mfr_id, foodbuy_mfr_name, reinhart_mfr_name
	from reinhart_all_mfr
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, reinhart_mfr_name ;			-- 1679 added


commit ;
select count(*) from master_mfr ;
	
	-- find all the ones to add to master from saladinos
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT  fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name
	from saladinos_mfr_match
	GROUP BY fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name ;					-- 499 added

-- created the master_mfr with 3891 rows
commit ;
select count(*) from master_mfr ;
