-- find all the ones to add to a master

-- bek_curr_mfr has 2585, so lets start with that for master
-- and add to it

DROP TABLE IF EXISTS master_mfr ;

create table  IF NOT EXISTS master_mfr(
  `foodbuy_mfr_id` varchar(15) DEFAULT NULL,
  `foodbuy_mfr_name` varchar(30) DEFAULT NULL,
  `vendor_num` varchar(20) DEFAULT NULL,
  `vendor_name` varchar(40) DEFAULT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;					-- complete
  

INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)
		SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, bek_mfr_name
		FROM bek_mfr
		GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, bek_mfr_name ;			-- 208

-- now get all the data from the rest

-- find all the ones to add to master from fsa_mfr_match
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name)
	SELECT DISTINCT  mfr_id, Manufacturer
	FROM fsa_mfr_match
	WHERE fsa_mfr_match.mfr_id not in
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr)		-- orig 594 found
	GROUP BY mfr_id, Manufacturer;								-- 594 added


	-- find all the ones to add to master from maines
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)	
	SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, maines_vendor_num, maines_vendor_name
	from maines_mfr
	where maines_mfr.foodbuy_mfr_id not in						-- orig found 42
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr) 
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, maines_vendor_num, maines_vendor_name;			-- 42 added
	
	
	-- find all the ones to add to master from merchants
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, merchants_mfr_num, merchants_mfr_name
	from merchants_mfr
	where merchants_mfr.foodbuy_mfr_id not in						-- orig found 102
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr) 
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, merchants_mfr_num, merchants_mfr_name ;			-- 102 added
		
	-- find all the ones to add to master from nicholas
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)		
	SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, nicholas_mfr_name
	from nicholas_mfr
	where nicholas_mfr.foodbuy_mfr_id not in					-- orig found 74
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr) 
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, nicholas_mfr_name ;			-- 74 added
	
	-- find all the ones to add to master from pfg_az
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, pfg_az_mfr_num, pfg_az_mfr_name
	from pfg_az_mfr
	where pfg_az_mfr.foodbuy_mfr_id not in					-- orig found 38
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr)
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, pfg_az_mfr_num, pfg_az_mfr_name ;			-- 38 added
 
	-- find all the ones to add to master from reinhart
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_name)		
	SELECT DISTINCT foodbuy_mfr_id, foodbuy_mfr_name, reinhart_mfr_name
	from reinhart_all_mfr
	where foodbuy_mfr_id not in					-- orig found 425
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr)
	GROUP BY foodbuy_mfr_id, foodbuy_mfr_name, reinhart_mfr_name ;			-- 425 added
	
	-- find all the ones to add to master from saladinos
INSERT INTO master_mfr
  (foodbuy_mfr_id, foodbuy_mfr_name, vendor_num, vendor_name)		
	SELECT DISTINCT fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name
	from saladinos_mfr_match
	where fb_mfr_id not in							-- orig found 352
		(SELECT DISTINCT foodbuy_mfr_id from bek_mfr)
	GROUP BY fb_mfr_id, fb_mfr_name, mfg_gln, mfg_name ;					-- 352 added

-- created the master_mfr with 1834 rows