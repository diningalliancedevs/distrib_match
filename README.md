# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: this is the ARAMARK monthly distributor match
* Version: v0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* load the Aramark pricing file
* load the distributor inventory files
* top level match, will match distributor inventory to aramark pricing table.
* Code is generic, but the tables have unique columns names, they aren't all the same.
* will have to expand to allow for distributors having diff column names.
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* the log file dumps out the sql.  when in doubt, take the sql and test.
* orig code pulled Nicholas inventory and and compared various fields, checked manufacturer info.
* time consuming, take values from each table and compare.
* Code review
* Other guidelines

### Who do I talk to? ###

* Sue Napier, Bill Cappoli or Leonard Linde
* Other community or team contact