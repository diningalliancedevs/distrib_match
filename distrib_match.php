<?php  
require "class/distrib_match_tool.php" ;

// distrib_match.php
// Top level of the New ARA Inventory match Tool,
// script given the name of distributor to 
// will determine what files need to get loaded

// expect script to be run like:
//	php distrib_match.php --distrib_name='maines'
// script will then determine the files to be used
// sdn Sept-Oct 2017


/*
Arguments in call are as follows:
argv[0] = name of script being run
argv[1] = --distrib_name=xxxxx
argv[2] = –-current_month=8
argv[3] = –-current_year=2017 
argv[4] = --test=1
*/
{						// main distrib_match	
	
    print "\n- - - - - - - - - - distrib_match - dump the parameters for log file- - - - - - - - - -\n" ;

	
	if (isset($argv[1])) {
		$report_param = $argv[1];

		//  If more than 1 command line arguments are provided at when script is run this code will call get_options() function to parse out options
		if(count($argv) > 1){	
			$options = get_options($argv);  		// from LL
		}

		if (count($options) < 3)
		{
			print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
			print " WARNING: Not enough parameter options retrieved. Abort!\n\n" ;
			print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;			
			print "	 params:   " ;
			print_r($argv) ;
			print "\n" ;	
			print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
			print " needs to have: --distrib_name=xxxxx –-current_month=xx –-current_year=20xx\n\n" ;
			die() ;
		}
		$distrib_name = $options['distrib_name'] ;		// add quotes req

		$year = '2017' ;		// default to this year
		$newyear = $options['current_year'] ;		// add quotes req
		if ($newyear != null)
			$year = $newyear ;
		
		$month = $options['current_month'] ;		// quotes req
		if ($month == null)
		{
			print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
			print " WARNING: no Month submit, will default to 1. Abort Now if necessary!\n\n" ;
			print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;			
			$month = '1';		
		}
		$use_date = Array ($month, $year) ;
				$tool = new distrib_match_tool() ;
		$test = $options['test'] ;		// for dev debug

		print "	 params:   " ;
		print_r($argv) ;
		print "\n" ;
		
		print " distrib_match: call start with $distrib_name, $month, $year.\n" ;
		$tool->start($distrib_name, $month, $year, $test) ;
	}
	else {		
		print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
		print "$argv[0] missing distrib_name argument, some params:\n " ;
		print "	  --distrib_name='capture' \n " ;
		print "   -–current_month=5 \n " ;
		print "   -–current_year=2017 \n\n " ;
		//print "  --test= 1 (range of 1)\n\n" ;

	   die("Specify report_name on command line\n\n");
	}
	
}// end of main function



/* 
 * get_options
 * This function parses the command line to get options that are passed to the script
 * when it is run from the command line
 * 1: argv: the command line parameters
 */
   function get_options($argv){  		// from LL
	$totalArgv = count($argv);
	$options[] = array();
	

	if ($totalArgv < 3)
	{
		print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
		print " WARNING: Not enough parameters. Abort!\n\n" ;
		print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;	
		print "	 params:   " ;
		print_r($argv) ;
		print "\n" ;
		print " * * * * * * * * * * * * * * * * * * * * * * *\n" ;
		print " needs to have: --distrib_name=xxxxx –-current_month=xx –-current_year=20xx\n\n" ;
		die() ;
	}

	for($x=1; $x < $totalArgv; $x++)
	{
		$trimmed = ltrim($argv[$x],'--');
		$opts = explode('=', $trimmed);
		$options[$opts[0]] = $opts[1];
	}

	// opts will have:
	// [0] => distrib_name
    // [1] => Nichols
	//
	// options will have:
	//  ['distrib_name'] => Nicholas
    //	['current_month'] => 8

	
	return $options ;
  }		// end of get_options function
  
 	
 
?>